#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "owl-pipeline-develop",
    "voluptuous",
]

setup_requirements = [
    "pytest-runner",
]

test_requirements = [
    "pytest>=3",
    "coverage",
    "pytest-cov",
    "pytest-mock",
]

setup(
    author="",
    author_email="",
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
    description="Python Boilerplate contains all the boilerplate you need to create a Python package.",
    install_requires=requirements,
    license="GNU General Public License v3 (GPLv3)",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="axio_mosaic_pipeline",
    name="axio-mosaic-pipeline",
    packages=find_packages(
        include=["axio_mosaic_pipeline", "axio_mosaic_pipeline.*"]
    ),
    entry_points={
        "owl.pipelines": "axio-mosaic = axio_mosaic_pipeline"
    },
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://github.com/IMAXT/axio_mosaic_pipeline",
    version="0.1.0",
    zip_safe=False,
    dependency_links=[
        "https://imaxt.ast.cam.ac.uk/pip/imaxt-image",
        "https://imaxt.ast.cam.ac.uk/pip/owl-pipeline-develop",
    ],
)
