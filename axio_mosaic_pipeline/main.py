from pathlib import Path

from owl_dev import pipeline
from owl_dev.logging import logger


@pipeline
def main(
    *,
    input_dir: Path = None,
    output_dir: Path = None,
    **kwargs,
):

    logger.info("Pipeline started")

    logger.info("Pipeline completed")
